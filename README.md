# *rTools README*

## Introduction

rTools is a suite of python scripts which enable you to administer multiple linux machines simultaneously. It consists of three scripts : 

* **rssh**
    * SSH into multiple machines, and execute commands on all the machines simultaneously
* **rgrep**
    * GREP log files across multiple machines simultaneously
* **rsql**
    * Command Line alternative to MySQL Workbench.

## Installing the Dependencies

**python-dev**  

    $ sudo apt-get install python-dev

**paramiko**  

    $ sudo pip install paramiko

**pycrypto**  

    $ sudo pip install pycrypto

**ecdsa**  

    $ sudo pip install ecdsa

## Installing rTools

Using the bundled setup.py script, you can create shell scripts which change directory to the code directory so relative links to the config files work, and then executes the corresponding python script. This shell script is installed to /usr/bin/ so that it's in your $PATH. 

    $ sudo python setup.py install rssh.py rssh
    $ sudo python setup.py install rgrep.py rgrep
    $ sudo python setup.py install rsql.py rsql

## Executing rTools

If you installed the scripts as explained above, you execute the following commands from anywhere.

    $ rssh
    $ rgrep
    $ rsql 

Otherwise, you need to navigate to the directory where you installed the files and execute the following commands

    $ python rssh.py
    $ python rgrep.py
    $ python rsql.py

Use the *--help* parameter to find out more about each of these scripts.

## Configuring rTools

Take a look at the default configuration files in the config folder. They are fairly self-explanatory, so just play around with the various options. Please note that the configuration files are written in strict JSON notation, so use a JSON validator like [JSONLint](http://jsonlint.com) to validate your configuration files.