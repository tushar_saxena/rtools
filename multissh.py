#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title       : multissh.py
# Author      : Tushar Saxena (tushar.saxena@gmail.com)
# Date        : 03-Feb-2012
# Description : Library for handling multiple SSH connections at the same time
# -----------------------------------------------------------------------------

import gzip
import json
import math
import os
import paramiko
import threading

import time

# --------------------------------------------------------------------------------------------------
class MultiSSH:
	# --------------------------------------------------------------------------------------------------

	remotes = None
	username = None
	password = None
	output_directory = None
	logger = None
	connection_container = dict()

	execute_counter = 0
	alias_max_length = 0

	# -------------------------------------
	def __init__(self, remotes, username, password, output_directory, logger):
		# -------------------------------------

		# Initialize class variables
		self.remotes = remotes
		self.username = username
		self.password = password
		self.output_directory = output_directory
		self.logger = logger

		# Create output directories
		if not os.path.exists(self.output_directory):
			os.makedirs(self.output_directory)

		# Check mandatory fields in remotes
		valid_keys = ['alias', 'host', 'port']
		invalid_entries = list()
		for i, remote in enumerate(remotes):
			for valid_key in valid_keys:
				if valid_key not in remote:
					self.logger.log('Incomplete remotes entry. Skipping => %s' % json.dumps(remote, sort_keys=True), self.logger.INFO)
					invalid_entries.append(i)
					continue
		for invalid_entry in invalid_entries:
			remotes.pop(invalid_entry)

		# Calculate max length of the alias/full hostname
		self.alias_max_length = 0
		self.full_hostname_max_length = 0
		for remote in remotes:
			if len(remote['alias']) > self.alias_max_length:
				self.alias_max_length = len(remote['alias'])
			full_hostname = '%s@%s:%d' % (username, remote['host'], remote['port'])
			if len(full_hostname) > self.full_hostname_max_length:
				self.full_hostname_max_length = len(full_hostname)

		# Create connection for every remote host
		for remote in remotes:
			connection = MultiSSHConnection(remote, username, password, self.alias_max_length, self.logger)
			self.connection_container[remote['alias']] = connection

		self.execute_counter = 0

	# -------------------------------------
	def connect(self):
		# -------------------------------------

		self.threadify('connect')

	# -------------------------------------
	def disconnect(self):
		# -------------------------------------

		self.threadify('disconnect')

	# -------------------------------------
	def execute(self, command):
		# -------------------------------------

		self.execute_counter += 1
		filename_container = self.threadify('execute', command)
		return filename_container, self.alias_max_length

	# -------------------------------------
	def status(self):
		# -------------------------------------

		for k, v in self.connection_container.items():
			status = 'Connected' if v.client else 'Not Connected'
			full_hostname = '%s@%s:%d' % (self.username, v.remote['host'], v.remote['port'])
			self.logger.log('%-*s | %-*s => %s' % (self.alias_max_length, v.remote['alias'], self.full_hostname_max_length, full_hostname, status), self.logger.STDOUT)

	# -------------------------------------
	def threadify(self, mode, command = None):
		# -------------------------------------

		thread_container = list()
		filename_container = dict()

		# Execute request
		for hostname in self.connection_container.iterkeys():
			connection = self.connection_container[hostname]
			filename = '%s/%s.%03d.log.gz' % (self.output_directory, hostname, self.execute_counter)
			filename_container[hostname] = filename
			connectionThread = MultiSSHConnectionThread(connection, mode, command, filename)
			connectionThread.start()
			thread_container.append(connectionThread)

		# Wait for threads to complete
		for thread in thread_container:
			thread.join()

		for hostname in filename_container.keys():
			if os.path.exists(filename_container[hostname]) == False:
				del filename_container[hostname]

		return filename_container

# --------------------------------------------------------------------------------------------------
class MultiSSHConnectionThread(threading.Thread):
	# --------------------------------------------------------------------------------------------------

	connection = None
	mode = None
	command = None
	filename = None

	# -------------------------------------
	def __init__(self, connection, mode, command, filename):
		# -------------------------------------

		# Initialize class variables
		self.connection = connection
		self.mode = mode
		self.command = command
		self.filename = filename

		threading.Thread.__init__(self)

	# -------------------------------------
	def run(self):
		# -------------------------------------

		if self.mode == 'execute':
			self.connection.execute(self.command, self.filename)
		elif self.mode == 'connect':
			self.connection.connect()
		elif self.mode == 'disconnect':
			self.connection.disconnect()

# --------------------------------------------------------------------------------------------------
class MultiSSHConnection:
	# --------------------------------------------------------------------------------------------------

	remote = None
	username = None
	password = None
	alias_max_length = None
	logger = None

	client = None
	stdin = None
	stdout = None
	stderr = None

	DEFAULT_SSH_PORT = 22

	# -------------------------------------
	def __init__(self, remote, username, password, alias_max_length, logger):
		# -------------------------------------

		# Initialize class variables
		self.remote = remote
		self.username = username
		self.password = password
		self.alias_max_length = alias_max_length
		self.logger = logger

		# Fix port
		if 'port' not in self.remote:
			self.remote['port'] = self.DEFAULT_SSH_PORT
		self.remote['port'] = int(self.remote['port'])

		# Initialize paramiko
		paramiko.util.log_to_file('logs/multissh.paramiko.log')
		self.client = paramiko.SSHClient()
		self.client.load_system_host_keys()
		# self.client.set_missing_host_key_policy(paramiko.WarningPolicy)
		self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

	# -------------------------------------
	def connect(self):
		# -------------------------------------

		try:
			self.logger.log('%-*s | Establishing connection : %s@%s:%s' % (self.alias_max_length, self.remote['alias'], self.username, self.remote['host'], self.remote['port']), self.logger.INFO)
			self.client.connect(self.remote['host'], self.remote['port'], self.username, self.password)
			self.logger.log('%-*s | Connection : OK' % (self.alias_max_length, self.remote['alias']), self.logger.INFO)
		except Exception, e:
			self.logger.log('%-*s | Connection : ERROR : %s (Removing from host list)' % (self.alias_max_length, self.remote['alias'], str(e)), self.logger.CRITICAL)
			self.client = None

	# -------------------------------------
	def disconnect(self):
		# -------------------------------------

		if self.client == None:
			return

		try:
			if self.client:
				self.client.close()
				self.client = None
			self.logger.log('%-*s | Disconnected : OK' % (self.alias_max_length, self.remote['alias']), self.logger.INFO)
		except Exception, e:
			self.logger.log('%-*s | Disconnected : ERROR : %s' % (self.alias_max_length, self.remote['alias'], str(e)), self.logger.CRITICAL)
			self.client = None

	# -------------------------------------
	def execute(self, cmd, filename):
		# -------------------------------------

		if self.client:
			t_start = time.time()

			# Handle sudo
			cmd_has_sudo = False
			if 'sudo ' in cmd:
				cmd_has_sudo = True
				cmd = cmd.replace('sudo ', 'sudo -S ')
				cmd = cmd.replace('sudo -S -S ', 'sudo -S ')

			# Send command
			try:
				self.logger.log('%-*s | Executing remote command : %s' % (self.alias_max_length, self.remote['alias'], cmd), self.logger.INFO)
				self.stdin, self.stdout, self.stderr = self.client.exec_command(cmd)
				if cmd_has_sudo:
					time.sleep(1)
					self.stdin.write('%s\n' % self.password)
					self.stdin.flush()
			except Exception, e:
				self.logger.log('%-*s | Communication error while sending request : %s (Removing from host list)' % (self.alias_max_length, self.remote['alias'], str(e)), self.logger.CRITICAL)
				self.client = None
				return None

			# Read response & write to file
			try:
				# Write to file
				file = gzip.open(filename, 'wb')
				file.write('$ %s\n' % cmd)  # Command being executed
				file_size_stdout = 0
				for file_size_stdout, line in enumerate(self.stdout, 1):
					if '[sudo]' not in line:
						file.write(line)
				file_size_stderr = 0
				for file_size_stderr, line in enumerate(self.stderr, 1):
					if '[sudo]' not in line:
						file.write(line)
				file_size = file_size_stdout + file_size_stderr
				file.close()
			except Exception, e:
				self.logger.log('%-*s | Communication error while writing response to file : %s (Removing from host list)' % (self.alias_max_length, self.remote['alias'], str(e)), self.logger.CRITICAL)
				self.client = None
				return None

			# Write summary line
			file_write_time = time.time() - t_start
			[fraction, integer] = math.modf(file_write_time)
			fraction *= 1000
			self.logger.log('%-*s | Execution time : %4d.%03d seconds | %6d lines returned | %s' % (self.alias_max_length, self.remote['alias'], integer, fraction, file_size, filename), self.logger.INFO)
