#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title       : rgrep.py
# Author      : Tushar Saxena (tushar.saxena@gmail.com)
# Date        : 29-Sep-2011
# Description : [remote grep] Python script to do a grep across all web servers
# -----------------------------------------------------------------------------

import getpass
import json
import os
import re
import sys

from loghelper import LogHelper
from multissh import MultiSSH
from optparse import OptionParser
from time import localtime, strftime

options = None
config = None
logger = None

configDict = {}

APP_NAME = 'rgrep'
TIMEFORMAT = '%Y%m%d.%H%M%S'
DEFAULT_CONFIG_FILENAME = 'config/%s.json' % (APP_NAME)
DEFAULT_LOG_FILENAME = 'logs/%s.log' % (APP_NAME)
DEFAULT_OUTPUT_BASE_DIR = 'output'
DEFAULT_OUTPUT_DIR = 'rssh'
DEFAULT_USERNAME = '$PROMPT$'
DEFAULT_PASSWORD = '$PROMPT$'

# -------------------------------------
def parseOptions():
	# -------------------------------------

	usage = 'Usage: %prog [options]'
	parser = OptionParser(usage)

	parser.add_option('-v', '--verbose', action='store_true', dest='verbose', default=False, help='Print status messages')
	parser.add_option('-c', '--config', dest='config', help='CONFIG file to use [default = %default]', metavar='CONFIG', default=DEFAULT_CONFIG_FILENAME)
	parser.add_option('-l', '--log', dest='log', help='LOG file to use [default = %default]', metavar='LOG', default=DEFAULT_LOG_FILENAME)

	(options, args) = parser.parse_args()

	if len(args) != 0:
		parser.print_help()
		sys.exit(1)

	return options

# -------------------------------------
def checkMandatoryFilesExist():
	# -------------------------------------

	flag = True
	mandatoryFiles = [options.config]

	for filename in mandatoryFiles:
		if not (os.path.exists(filename)):
			print '%s | %s does not exist' % (APP_NAME, filename)
			flag = False

	if flag == False:
		print '%s | Exiting ...' % (APP_NAME)
		sys.exit(3);

# -------------------------------------
def parseConfig():
	# -------------------------------------

	if os.path.exists(options.config):
		with open(options.config) as json_data_file:
			config = json.load(json_data_file)
			print '%s | Loaded config file <%s>' % (APP_NAME, options.config)
			return config

	print '%s | Error loading config file <%s>. Exiting ...' % (APP_NAME, options.config)
	sys.exit(2)

# -------------------------------------
def getLoginCredentials(username, password):
	# -------------------------------------

	# Get username
	if username.upper() == '$PROMPT$':
		username = raw_input('%s | username [%s]: ' % (APP_NAME, getpass.getuser()))
		if username.strip() == '':
			username = getpass.getuser()
	else:
		logger.log('%s | Using %s as the username' % (APP_NAME, username), logger.INFO)

	# Get password
	if password.upper() == '$PROMPT$':
		password = getpass.getpass('%s | password: ' % (APP_NAME))
	else:
		logger.log('%s | Using password stored in config file' % (APP_NAME), logger.INFO)

	return (username, password)

# -------------------------------------
def buildRemoteQuery():
	# -------------------------------------

	DEFAULT_SUDO = False
	DEFAULT_GREP_COMMAND = 'grep'
	DEFAULT_GREP_OPTIONS = ''
	DEFAULT_REGEX = 'hello world'
	DEFAULT_FILE_PATH = '.'
	DEFAULT_FILE_PATTERN = '*'

	# Validate query parameters
	sudo = config['search']['sudo'] if 'search' in config and 'sudo' in config['search'] else DEFAULT_SUDO
	grep_command = config['search']['grep']['command'] if 'search' in config and 'grep' in config['search'] and 'command' in config['search']['grep'] else DEFAULT_GREP_COMMAND
	grep_options = config['search']['grep']['options'] if 'search' in config and 'grep' in config['search'] and 'options' in config['search']['grep'] else DEFAULT_GREP_OPTIONS
	regex = config['search']['regex'] if 'search' in config and 'regex' in config['search'] else DEFAULT_REGEX
	file_path = config['search']['file']['path'] if 'search' in config and 'file' in config['search'] and 'path' in config['search']['file'] else DEFAULT_FILE_PATH
	file_pattern = config['search']['file']['pattern'] if 'search' in config and 'file' in config['search'] and 'pattern' in config['search']['file'] else DEFAULT_FILE_PATTERN

	# Build remote query
	cmd = ''
	if sudo:
		cmd += 'sudo -S '
	cmd += '%s %s --regexp=\'%s\' %s' % (grep_command, grep_options, regex, os.path.join(file_path, file_pattern))

	return cmd

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------

	# Parse command line args
	options = parseOptions()

	# Check if mandatory files exist
	checkMandatoryFilesExist()

	# Parse config file
	config = parseConfig()

	# Start Logging
	logger = LogHelper(options.log, options.verbose)
	logger.log('%s | Initializing new session' % (APP_NAME), logger.INFO)
	logger.log('%s | (c) 2012 Tushar Saxena (tushar.saxena@gmail.com)' % (APP_NAME), logger.STDOUT)

	# Get login credentials
	username = config['login']['username'] if 'login' in config and 'username' in config['login'] else DEFAULT_USERNAME
	password = config['login']['password'] if 'login' in config and 'password' in config['login'] else DEFAULT_PASSWORD
	username, password = getLoginCredentials(username, password)

	# Build remote grep query
	cmd = buildRemoteQuery()

	# Generate output directory name
	timestamp = strftime(TIMEFORMAT, localtime())
	output_base_dir = config['output']['base_dir'] if 'output' in config and 'base_dir' in config['output'] else DEFAULT_OUTPUT_BASE_DIR
	output_dir = config['output']['dir'] if 'output' in config and 'dir' in config['output'] else DEFAULT_OUTPUT_DIR
	output_directory = os.path.join(config['output']['base_dir'], config['output']['dir'], timestamp)
	logger.log('%s | Output directory : %s' % (APP_NAME, output_directory), logger.INFO)

	# Create remote connections
	mssh = MultiSSH(config['remotes'], username, password, output_directory, logger)

	# Execute remote query
	mssh.connect()
	mssh.execute(cmd)
	mssh.disconnect()
	view_cmd = 'zcat %s/*' % output_directory
	logger.log('%s | View output : %s' % (APP_NAME, view_cmd), logger.STDOUT)

	# Stop Logging
	logger.log('%s | Terminating session' % (APP_NAME), logger.INFO)
	logger.close()

	# Exit
	sys.exit(0)
