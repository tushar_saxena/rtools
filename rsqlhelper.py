#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title         : rsqlhelper.py
# Author        : Tushar Saxena (tushar.saxena@gmail.com)
# Date          : 03-Feb-2012
# Description   : Library for handling remote sql connection in an interactive
# terminal
# -----------------------------------------------------------------------------

import fcntl
import os
import paramiko
import select
import socket
import struct
import sys
import termios
import tty

from activitymonitor import ActivityMonitor

APP_NAME = 'rsql'

# --------------------------------------------------------------------------------------------------
class RSQLHelper:
	# --------------------------------------------------------------------------------------------------

	ssh_proxy_host = None
	ssh_proxy_port = None
	ssh_proxy_user = None
	ssh_proxy_password = None

	mysql_host = None
	mysql_port = None
	mysql_user = None
	mysql_password = None
	mysql_database = None
	mysql_options = None

	logger = None
	timeout = None

	client = None
	channel = None
	mysql_command = None

	tty_rows = None
	tty_cols = None

	activityMonitor = None

	DEFAULT_SSH_PORT = 22
	DEFAULT_MYSQL_PORT = 3306
	DEFAULT_MYSQL_OPTIONS = ''
	BUFFER_SIZE = 4096
	DEFAULT_TIMEOUT = 600

	# -------------------------------------
	def __init__(self, remote, logger):
		# -------------------------------------

		# Initialize class variables
		self.ssh_proxy_host = remote['ssh_proxy']['host']
		self.ssh_proxy_port = remote['ssh_proxy']['port']
		self.ssh_proxy_user = remote['ssh_proxy']['username']
		self.ssh_proxy_password = remote['ssh_proxy']['password']

		self.mysql_host = remote['mysql']['host']
		self.mysql_port = remote['mysql']['port']
		self.mysql_user = remote['mysql']['username']
		self.mysql_password = remote['mysql']['password']
		self.mysql_database = remote['mysql']['database']
		self.mysql_options = remote['mysql']['options'] if 'mysql' in remote and 'options' in remote['mysql'] else self.DEFAULT_MYSQL_OPTIONS

		self.logger = logger
		# TODO: Get timeout from config
		self.timeout = self.DEFAULT_TIMEOUT

		# Build MySQL command
		self.mysql_command = self.build_mysql_command()

		# Initialize paramiko
		paramiko.util.log_to_file('logs/rsqlhelper.paramiko.log')
		self.client = paramiko.SSHClient()
		self.client.load_system_host_keys()
		# self.client.set_missing_host_key_policy(paramiko.WarningPolicy)
		self.client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

		# Register Activity Monitor
		self.activityMonitor = ActivityMonitor(self, 'timeout_callback', self.timeout)
		self.activityMonitor.start()

	# -------------------------------------
	def timeout_callback(self):
		# -------------------------------------

		self.logger.log('%s | Connection timed out' % (APP_NAME), self.logger.INFO)
		self.disconnect()

	# -------------------------------------
	def connect(self):
		# -------------------------------------
		try:
			self.logger.log('%s | Creating SSH tunnel : %s@%s:%s' % (APP_NAME, self.ssh_proxy_user, self.ssh_proxy_host, self.ssh_proxy_port), self.logger.INFO)
			self.client.connect(self.ssh_proxy_host, self.ssh_proxy_port, self.ssh_proxy_user, self.ssh_proxy_password)
			self.channel = self.client.invoke_shell()
			self.interactive_shell()
		except Exception, e:
			print 'rsql | %s' % (str(e))
			self.activityMonitor.terminateActivity()
			self.client = None
			self.channel = None

	# -------------------------------------
	def interactive_shell(self):
		# -------------------------------------

		try:
			# Backup current terminal settings
			oldtty = termios.tcgetattr(sys.stdin)

			# Set terminal settings
			tty.setraw(sys.stdin.fileno())
			tty.setcbreak(sys.stdin.fileno())
			self.channel.settimeout(0.0)

			# Resize terminal to match current settings
			self.resizeTTY()

			# Send MySQL command
			self.logger.log('%s | Connecting to MySQL instance : %s@%s:%d' % (APP_NAME, self.mysql_user, self.mysql_host, self.mysql_port), self.logger.INFO)
			print '\r'
			self.channel.send(self.mysql_command)

			displayOutput = True
			while True:
				# Resize terminal
				# self.resizeTTY()

				# Select
				r, w, e = select.select([self.channel, sys.stdin], [], [])

				# Read input from channel and write to stdout
				if self.channel in r:
					try:
						x = self.channel.recv(self.BUFFER_SIZE)
						# TODO: Fix this mess
						if x[0:3] == 'Bye':
							print '\r'
							self.activityMonitor.terminateActivity()
							break
						if x[0:8] == '^CCtrl-C':
							print '\r'
							self.activityMonitor.terminateActivity()
							break
						if x[2:5] == 'Bye':
							print '\r'
							self.activityMonitor.terminateActivity()
							break
						if len(x) == 0:
							print '\r'
							self.activityMonitor.terminateActivity()
							break
						if x[0:29] == 'Welcome to the MySQL monitor.':
							displayOutput = True
						if displayOutput == True:
							sys.stdout.write('%s' % x)
							sys.stdout.flush()
					except socket.timeout:
						pass

				# Read input from stdin and send to channel
				if sys.stdin in r:
					x = os.read(sys.stdin.fileno(), 1)
					if len(x) == 0:
						break
					self.channel.send(x)
					self.activityMonitor.registerActivity()
		except Exception, e:
			self.logger.log('rsql | %s' % str(e), self.logger.DEBUG)
			self.activityMonitor.terminateActivity()
			self.client = None
			self.channel = None
		finally:
			# Restore original terminal settings
			termios.tcsetattr(sys.stdin, termios.TCSADRAIN, oldtty)

	# -------------------------------------
	def resizeTTY(self):
		# -------------------------------------

		# TODO: WTF does this do?
		[rows, cols, xpix, ypix] = struct.unpack("HHHH", fcntl.ioctl(sys.stdout.fileno(), termios.TIOCGWINSZ, struct.pack("HHHH", 0, 0, 0, 0)))

		if (rows != self.tty_rows or cols != self.tty_cols):
			self.logger.log('rsql | Resizing terminal to (%d,%d)' % (cols, rows), self.logger.DEBUG)
			self.channel.resize_pty(width=cols, height=rows)
			self.tty_rows = rows
			self.tty_cols = cols

	# -------------------------------------
	def disconnect(self):
		# -------------------------------------

		try:
			if self.channel:
				self.channel.close()
				self.channel = None
			if self.client:
				self.client.close()
				self.client = None
		except Exception, e:
			self.logger.log('%s | %s' % (APP_NAME, str(e)), self.logger.DEBUG)
			self.activityMonitor.terminateActivity()

	# -------------------------------------
	def build_mysql_command(self):
		# -------------------------------------

		mysql_command = '\rmysql --host=%s --port=%s --user=%s --password=%s --database=%s' % (self.mysql_host, self.mysql_port, self.mysql_user, self.mysql_password, self.mysql_database)

		if len(self.mysql_options) > 0:
			mysql_command += ' %s' % (self.mysql_options)
		mysql_command += '\n'

		return mysql_command
