#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title       : activitymonitor.py
# Author      : Tushar Saxena (tushar.saxena@gmail.com)
# Date        : 29-Jun-2011
# Description : Generic activity monitoring library
# -----------------------------------------------------------------------------

import threading
import time

# --------------------------------------------------------------------------------------------------
class ActivityMonitor(threading.Thread):
	# --------------------------------------------------------------------------------------------------

	SLEEP_TIME = 1

	last_activity_time = None
	__isActive = None
	timeout = None
	callbackObject = None
	callbackMethod = None

	# -------------------------------------
	def __init__(self, callbackObject, callbackMethod, timeout = 600):
		# -------------------------------------

		# Initialize class variables
		self.timeout = timeout
		self.last_activity_time = time.time()
		self.__isActive = True
		self.callbackObject = callbackObject
		self.callbackMethod = callbackMethod

		threading.Thread.__init__(self)

	# -------------------------------------
	def run(self):
		# -------------------------------------

		while (self.__isActive):
			current_time = time.time()
			if (current_time - self.last_activity_time) >= self.timeout:
				# Timeout occurred - Invoke callback function
				getattr(self.callbackObject, self.callbackMethod)()
				return
			else:
				# No timeout
				time.sleep(self.SLEEP_TIME)

	# -------------------------------------
	def registerActivity(self):
		# -------------------------------------

		self.last_activity_time = time.time()

	# -------------------------------------
	def terminateActivity(self):
		# -------------------------------------

		self.__isActive = False

	# -------------------------------------
	def isActive(self):
		# -------------------------------------

		return self.__isActive