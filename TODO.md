# *rTools TODO*

## TODO

* general : Handle JSOn parsing errors gracefully
* rsql : Make SSH_PROXY optional (and move it to a separate section in the config)
* rgrep : Override file parameters with command line parameters
* rsql : Don't run in interactive mode
* rsql : Hide the command used to invoke MySQL once connected to proxy box (mysql --host=localhost --port=3306 --user=??? --password=??? --database=taxi_dispatch --no-auto-rehash)
* general : Handle verbose mode better

## DONE

* general : Move to JSON configuration
* general : Add hostname alias as a config parameter and display that everywhere
* general : Fix file naming
* rssh : Handle sudo (http://jessenoller.com/2009/02/05/ssh-programming-with-paramiko-completely-different/)
* rgrep : Handle sudo
* rsql : Move rsqlhlp.py into main MySQL class (maybe)
* rgrep : Add sudo parameter
* rgrep : Handle zipped logs (zgrep)
