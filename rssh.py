#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title       : rssh.py
# Author      : Tushar Saxena (tushar.saxena@gmail.com)
# Date        : 29-Sep-2011
# Description : Python script to do execute remote commands a cluster of hosts
# -----------------------------------------------------------------------------

import atexit
import getpass
import gzip
import json
import os
import readline
import sys

from activitymonitor import ActivityMonitor
from loghelper import LogHelper
from multissh import MultiSSH
from optparse import OptionParser
from time import localtime, strftime

options = None
config = None
logger = None

line_limit = None
timeout = None

APP_NAME = 'rssh'
TIMEFORMAT = '%Y%m%d.%H%M%S'
DEFAULT_CONFIG_FILENAME = 'config/%s.json' % (APP_NAME)
DEFAULT_LOG_FILENAME = 'logs/%s.log' % (APP_NAME)
DEFAULT_HISTORY_FILE = '.rssh.history'
DEFAULT_TIMEOUT = 600
DEFAULT_LINE_LIMIT = 10
DEFAULT_OUTPUT_BASE_DIR = 'output'
DEFAULT_OUTPUT_DIR = 'rgrep'
DEFAULT_USERNAME = '$PROMPT$'
DEFAULT_PASSWORD = '$PROMPT$'

# -------------------------------------
def parseOptions():
	# -------------------------------------

	usage = 'Usage: %prog [options]'
	parser = OptionParser(usage)

	parser.add_option('-v', '--verbose', action='store_true', dest='verbose', default=False, help='Print status messages')
	parser.add_option('-c', '--config', dest='config', help='CONFIG file to use [default = %default]', metavar='CONFIG', default=DEFAULT_CONFIG_FILENAME)
	parser.add_option('-l', '--log', dest='log', help='LOG file to use [default = %default]', metavar='LOG', default=DEFAULT_LOG_FILENAME)

	(options, args) = parser.parse_args()

	if len(args) != 0:
		parser.print_help()
		sys.exit(1)

	return options

# -------------------------------------
def checkMandatoryFilesExist():
	# -------------------------------------

	flag = True
	mandatoryFiles = [options.config]

	for filename in mandatoryFiles:
		if not (os.path.exists(filename)):
			print '%s | <%s> does not exist' % (APP_NAME, filename)
			flag = False

	if flag == False:
		print '%s | Exiting ...' % (APP_NAME)
		sys.exit(3);

# -------------------------------------
def parseConfig():
	# -------------------------------------

	if os.path.exists(options.config):
		with open(options.config) as json_data_file:
			config = json.load(json_data_file)
			print '%s | Loaded config file <%s>' % (APP_NAME, options.config)
			return config

	print '%s | Error loading config file <%s>. Exiting ...' % (APP_NAME, options.config)
	sys.exit(2)

# -------------------------------------
def getLoginCredentials(username, password):
	# -------------------------------------

	# Get username
	if username.upper() == '$PROMPT$':
		username = raw_input('%s | username [%s]: ' % (APP_NAME, getpass.getuser()))
		if username.strip() == '':
			username = getpass.getuser()
	else:
		logger.log('%s | Using %s as the username' % (APP_NAME, username), logger.INFO)

	# Get password
	if password.upper() == '$PROMPT$':
		password = getpass.getpass('%s | password: ' % (APP_NAME))
	else:
		logger.log('%s | Using password stored in config file' % (APP_NAME), logger.INFO)

	return (username, password)

# --------------------------------------------------------------------------------------------------
class CommandPrompt:
	# --------------------------------------------------------------------------------------------------

	activityMonitor = None

	predefined_command_dict = None
	config_command_dict = None
	variable_dict = None

	cmd = None
	cmdTokens = None
	firstToken = None
	history_file = None

	config_command_max_length = None
	variable_max_length = None

	# -------------------------------------
	def __init__(self):
		# -------------------------------------

		# Build command lists
		self.config_command_dict = self.buildConfigCommands()
		self.predefined_command_dict = self.buildPredefinedCommands()
		self.variable_dict = self.buildVariableCommands()

		# Get max lengths
		self.config_command_max_length = len(max(self.config_command_dict, key=len))
		self.variable_max_length = len(max(self.variable_dict, key=len))

		# Read History File
		try:
			self.history_file = DEFAULT_HISTORY_FILE
			atexit.register(readline.write_history_file, self.history_file)
			readline.read_history_file(self.history_file)
		except IOError:
			pass

		# Start activity monitor
		self.activityMonitor = ActivityMonitor(self, 'timeout_callback', timeout)
		self.activityMonitor.start()

	# -------------------------------------
	def executeCommand(self, cmd):
		# -------------------------------------

		# Execute command
		filename_container, hostname_max_length = mssh.execute(cmd)

		# Display output
		for hostname, filename in filename_container.iteritems():
			if os.path.exists(filename):
				file = gzip.open(filename)
				line_counter = 0
				headerString = '[ %-*s ] %s' % (hostname_max_length, hostname, ('-' * (80 - hostname_max_length - 5)))
				logger.log(headerString, logger.STDOUT)
				for line in file:
					line_counter += 1
					ln_lmt = int(self.variable_dict['line_limit'])
					if line_counter > ln_lmt and ln_lmt != 0 and ln_lmt != None:
						logger.log('.....', logger.STDOUT)
						break
					elif line_counter > 1:
						logger.log('%s' % (line[:-1]), logger.STDOUT)
				file.close()

		logger.log('-' * 80, logger.STDOUT)

	# -------------------------------------
	def execute(self):
		# -------------------------------------

		# Loop
		while (True):

			self.cmd = None
			self.cmdTokens = None
			self.firstToken = None

			try:

				# Accept input
				self.cmd = raw_input('%s $ ' % (APP_NAME)).strip()

				# Check if activity monitor is alive
				if not (self.activityMonitor and self.activityMonitor.isActive()):
					break

				# Register activity
				self.activityMonitor.registerActivity()

				if len(self.cmd) > 0:

					# Tokenize command
					self.cmdTokens = self.cmd.split(' ')

					if self.cmdTokens[0][0] == ':':
						# :command
						self.firstToken = self.cmdTokens[0][1:]
						# Check if command exists in predefined_command_dict
						if self.firstToken in self.predefined_command_dict.keys():
							func = self.predefined_command_dict[self.firstToken]['id']
							getattr(self, func)()
						# Check if command exists in config_command_dict
						elif self.firstToken in self.config_command_dict.keys():
							config_cmd = self.config_command_dict[self.firstToken]['command']
							logger.log('%s $ %s' % (APP_NAME, config_cmd), logger.STDOUT)
							self.executeCommand(config_cmd)
						else:
							logger.log('Invalid command', logger.STDOUT)
					elif self.cmdTokens[0] == 'exit':
						self.cmd_exit()
					else:
						# Execute command
						self.executeCommand(self.cmd)
			except KeyboardInterrupt, e:
				logger.log('Keyboard Interrupt', logger.CRITICAL)
				if self.activityMonitor:
					self.activityMonitor.terminateActivity()
				exit()
			except Exception, e:
				logger.log('Fatal Error : %s' % (e), logger.CRITICAL)
				if self.activityMonitor:
					self.activityMonitor.terminateActivity()
				exit()

	# -------------------------------------
	def timeout_callback(self):
		# -------------------------------------

		logger.log('\n%s | Connection timed out' % (APP_NAME), logger.STDOUT)
		mssh.disconnect()
		self.activityMonitor = None

	# -------------------------------------
	def buildPredefinedCommands(self):
		# -------------------------------------

		dict = {
		'help': {
		'id': 'cmd_help',
		'display': 'Display this help text'
		},
		'hosts': {
		'id': 'cmd_hosts',
		'display': 'Show all connected hosts'
		},
		'commands': {
		'id': 'cmd_commands',
		'display': 'List all commands'
		},
		'command': {
		'id': 'cmd_command',
		'display': 'Show command details (:command <command>)'
		},
		'set': {
		'id': 'cmd_set',
		'display': 'Set variable (:set <variable> <value>)'
		},
		'list': {
		'id': 'cmd_list',
		'display': 'List variables'
		},
		'exit': {
		'id': 'cmd_exit',
		'display': 'Exit'
		},
		}

		return dict

	# -------------------------------------
	def buildVariableCommands(self):
		# -------------------------------------

		dict = {}

		dict['line_limit'] = line_limit
		# dict['timeout'] = timeout

		return dict

	# -------------------------------------
	def buildConfigCommands(self):
		# -------------------------------------

		dict = {}

		for command in config['commands']:
			dict[command['id']] = {
			'display_name': command['name'],
			'command': command['command']
			}

		return dict

	# -------------------------------------
	def cmd_help(self):
		# -------------------------------------

		if len(self.cmdTokens) == 1:
			for k in sorted(self.predefined_command_dict.iterkeys()):
				print ':%-8s => %s' % (k, self.predefined_command_dict[k]['display'])
		else:
			logger.log('Usage ==> :help', logger.STDOUT)

	# -------------------------------------
	def cmd_hosts(self):
		# -------------------------------------

		if len(self.cmdTokens) == 1:
			mssh.status()
		else:
			logger.log('Usage ==> :hosts', logger.STDOUT)

	# -------------------------------------
	def cmd_commands(self):
		# -------------------------------------

		if len(self.cmdTokens) == 1:
			for key in sorted(self.config_command_dict.iterkeys()):
				display_name = self.config_command_dict[key]['display_name']
				logger.log('%-*s | %s' % (self.config_command_max_length, key, display_name), logger.STDOUT)
		else:
			logger.log('Usage ==> :commands', logger.STDOUT)

	# -------------------------------------
	def cmd_command(self):
		# -------------------------------------

		if len(self.cmdTokens) == 2:
			key = self.cmdTokens[1]
			if key in self.config_command_dict.keys():
				display_name = self.config_command_dict[key]['display_name']
				command = self.config_command_dict[key]['command']
				logger.log('%s ==> %s' % (display_name, command), logger.STDOUT)
			else:
				logger.log('Invalid command \'%s\'' % (key), logger.STDOUT)
		else:
			logger.log('Usage ==> :command <command>', logger.STDOUT)

	# -------------------------------------
	def cmd_exit(self):
		# -------------------------------------

		if len(self.cmdTokens) == 1:
			if self.activityMonitor:
				self.activityMonitor.terminateActivity()
			exit()

	# -------------------------------------
	def cmd_set(self):
		# -------------------------------------

		if len(self.cmdTokens) == 3:
			variable = self.cmdTokens[1]
			value = self.cmdTokens[2]
			if variable in self.variable_dict.keys():
				self.variable_dict[variable] = value
				logger.log('%s ==> %s' % (variable, value), logger.STDOUT)
			else:
				logger.log('Invalid variable \'%s\'' % (variable), logger.STDOUT)
		else:
			logger.log('Usage ==> :set <variable> <value>', logger.STDOUT)

	# -------------------------------------
	def cmd_list(self):
		# -------------------------------------

		if len(self.cmdTokens) == 1:
			for k, v in self.variable_dict.iteritems():
				logger.log('%-*s | %s' % (self.variable_max_length, k, v), logger.STDOUT)
		else:
			logger.log('Usage ==> :list', logger.STDOUT)

# -------------------------------------
def exit():
	# -------------------------------------

	# Disconnect
	mssh.disconnect()

	# Stop Logging
	logger.log('%s | Terminating session' % (APP_NAME), logger.INFO)
	logger.close()

	# Exit
	sys.exit(0)

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------

	# Parse command line args
	options = parseOptions()

	# Check if mandatory files exist
	checkMandatoryFilesExist()

	# Parse config file
	config = parseConfig()

	# Start Logging
	logger = LogHelper(options.log, options.verbose)
	logger.log('%s | Initializing new session' % (APP_NAME), logger.INFO)
	logger.log('%s | (c) 2012 Tushar Saxena (tushar.saxena@gmail.com)' % (APP_NAME), logger.STDOUT)

	# Get login credentials
	username = config['login']['username'] if 'login' in config and 'username' in config['login'] else DEFAULT_USERNAME
	password = config['login']['password'] if 'login' in config and 'password' in config['login'] else DEFAULT_PASSWORD
	username, password = getLoginCredentials(username, password)

	# Generate output directory name
	timestamp = strftime(TIMEFORMAT, localtime())
	output_base_dir = config['output']['base_dir'] if 'output' in config and 'base_dir' in config['output'] else DEFAULT_OUTPUT_BASE_DIR
	output_dir = config['output']['dir'] if 'output' in config and 'dir' in config['output'] else DEFAULT_OUTPUT_DIR
	output_directory = os.path.join(config['output']['base_dir'], config['output']['dir'], timestamp)
	logger.log('%s | Output directory : %s' % (APP_NAME, output_directory), logger.INFO)

	# Get line limit
	line_limit = int(config['output']['line_limit']) if 'output' in config and 'line_limit' in config['output'] else DEFAULT_LINE_LIMIT
	logger.log('%s | Output line limit : %s lines' % (APP_NAME, line_limit), logger.INFO)

	# Get timeout
	timeout = int(config['general']['timeout']) if 'general' in config and 'timeout' in config['general'] else DEFAULT_TIMEOUT
	logger.log('%s | Timeout : %s seconds' % (APP_NAME, timeout), logger.INFO)

	# Create remote connections
	mssh = MultiSSH(config['remotes'], username, password, output_directory, logger)
	mssh.connect()

	# Command prompt
	logger.log('%s | Connected. Type \':help\' for list of available commands' % (APP_NAME), logger.STDOUT)
	cp = CommandPrompt()
	cp.execute()

	# Exit
	exit()
