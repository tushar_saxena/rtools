#!/usr/bin/env python

# -----------------------------------------------------------------------------
# Title         : rsql.py
# Author        : Tushar Saxena (tushar.saxena@gmail.com)
# Date          : 20-Feb-2012
# Description   : Python script to connect to predefined mysql databases via a
# SSH proxy
# -----------------------------------------------------------------------------

import getpass
import json
import os
import sys

from loghelper import LogHelper
from optparse import OptionParser
from rsqlhelper import RSQLHelper

options = None
config = None
logger = None

APP_NAME = 'rsql'
DEFAULT_CONFIG_FILENAME = 'config/%s.json' % (APP_NAME)
DEFAULT_LOG_FILENAME = 'logs/%s.log' % (APP_NAME)

# -------------------------------------
def parseOptions():
	# -------------------------------------

	usage = 'Usage: %prog [options]'
	parser = OptionParser(usage)

	parser.add_option('-v', '--verbose', action='store_true', dest='verbose', default=False, help='Print status messages')
	parser.add_option('-c', '--config', dest='config', help='CONFIG file to use [default = %default]', metavar='CONFIG', default=DEFAULT_CONFIG_FILENAME)
	parser.add_option('-l', '--log', dest='log', help='LOG file to use [default = %default]', metavar='LOG', default=DEFAULT_LOG_FILENAME)
	parser.add_option('-e', '--environment', dest='environment', help='Select a preconfigured environment to connect to [default = %default]', default=None)
	parser.add_option('-t', '--list', action='store_true', dest='list', default=False, help='List out all pre-configured environments to connect to [default = %default]')

	(options, args) = parser.parse_args()

	if len(args) != 0:
		parser.print_help()
		sys.exit(1)

	return options

# -------------------------------------
def checkMandatoryFilesExist():
	# -------------------------------------

	flag = True
	mandatoryFiles = [options.config]

	for filename in mandatoryFiles:
		if not (os.path.exists(filename)):
			print '%s | <%s> does not exist' % (APP_NAME, filename)
			flag = False

	if flag == False:
		print '%s | Exiting ...' % (APP_NAME)
		sys.exit(3);

# -------------------------------------
def parseConfig():
	# -------------------------------------

	if os.path.exists(options.config):
		with open(options.config) as json_data_file:
			config = json.load(json_data_file)
			print '%s | Loaded config file <%s>' % (APP_NAME, options.config)
			return config

	print '%s | Error loading config file <%s>. Exiting ...' % (APP_NAME, options.config)
	sys.exit(2)

# -------------------------------------
def getLoginCredentials(username, password, usage):
	# -------------------------------------

	# Get username
	if username.upper() == '$PROMPT$':
		username = raw_input('%s | %s username [%s]: ' % (APP_NAME, usage, getpass.getuser()))
		if username.strip() == '':
			username = getpass.getuser()
	else:
		logger.log('%s | Using %s as the %s username' % (APP_NAME, username, usage), logger.INFO)

	# Get password
	if password.upper() == '$PROMPT$':
		password = getpass.getpass('%s | %s password: ' % (APP_NAME, usage))
	else:
		logger.log('%s | Using %s password stored in config file' % (APP_NAME, usage), logger.INFO)

	return ([username, password])

# -------------------------------------
def validateConnections(remotes):
	# -------------------------------------

	valid_keys = [['id'], ['alias'], ['ssh_proxy', 'host'], ['ssh_proxy', 'port'], ['ssh_proxy', 'username'], ['ssh_proxy', 'password'], ['mysql', 'host'], ['mysql', 'port'], ['mysql', 'username'], ['mysql', 'password']]
	invalid_entries = list()
	for i, remote in enumerate(remotes):
		for valid_key in valid_keys:
			if (len(valid_key) == 1 and valid_key[0] not in remote) or (len(valid_key) == 2 and (valid_key[0] not in remote or valid_key[1] not in remote[valid_key[0]])):
				logger.log('%s | Incomplete remotes entry. Skipping => %s' % (APP_NAME, json.dumps(remote, sort_keys=True)), logger.INFO)
				invalid_entries.append(i)
				continue

	for invalid_entry in invalid_entries:
		remotes.pop(invalid_entry)

	return remotes

# -------------------------------------
def selectConnection(remotes):
	# -------------------------------------

	selected_connection = None

	# Handle --list mode
	if options.list:
		logger.log('%s | Available connections :' % (APP_NAME), logger.STDOUT)
		for connection in remotes:
			logger.log('%s | %-25s - %s' % (APP_NAME, connection['id'], connection['alias']), logger.STDOUT)
		exit()

	# Handle --environment mode
	if options.environment:
		# Check if selected environment exists
		filtered_remotes = filter(lambda x: x['id'] == options.environment, remotes)
		if len(filtered_remotes) == 0:
			# Does not exist - exit
			logger.log('%s | %s is not a valid environment, use --list for a list of available connections' % (APP_NAME, options.environment), logger.STDOUT)
			exit()
		else:
			selected_connection = filtered_remotes[0]['id']
	else:
		# Display available connections
		logger.log('%s | Available connections :' % (APP_NAME), logger.STDOUT)
		for index, remote in enumerate(remotes):
			logger.log('%s | %d. %s' % (APP_NAME, (index + 1), remote['alias']), logger.STDOUT)

		# Accept selection from user
		while True:
			selection = None
			try:
				selection = raw_input('%s | Select a connection : ' % (APP_NAME))
				selected_connection_id = int(selection)
				selected_connection = remotes[selected_connection_id - 1]['id']
				break
			except:
				if selection:
					pass
				else:
					exit()

	# Get login credentials for SSH proxy and MySQL
	remote = filter(lambda x: x['id'] == selected_connection, remotes)[0]
	remote['ssh_proxy']['username'], remote['ssh_proxy']['password'] = getLoginCredentials(remote['ssh_proxy']['username'], remote['ssh_proxy']['password'], 'SSH proxy')
	remote['mysql']['username'], remote['mysql']['password'] = getLoginCredentials(remote['mysql']['username'], remote['mysql']['password'], 'MySQL')

	# Return remote object
	return remote

# -------------------------------------
def exit():
	# -------------------------------------

	# Stop Logging
	logger.log('%s | Terminating rsql session' % (APP_NAME), logger.INFO)
	logger.close()

	# Exit
	sys.exit(0)

# -------------------------------------
if __name__ == '__main__':
	# -------------------------------------

	# Parse command line args
	options = parseOptions()

	# Check if mandatory files exist
	checkMandatoryFilesExist()

	# Parse config file
	config = parseConfig()

	# Start Logging
	logger = LogHelper(options.log, options.verbose)
	logger.log('%s | Initializing new session' % (APP_NAME), logger.INFO)
	logger.log('%s | (c) 2012 Tushar Saxena (tushar.saxena@gmail.com)' % (APP_NAME), logger.STDOUT)

	# Validate connections
	remotes = validateConnections(config['remotes'])

	# Select Connection
	remote = selectConnection(remotes)

	# Establish connection
	rsql_connection = RSQLHelper(remote, logger)
	rsql_connection.connect()
	rsql_connection.disconnect()

	# Exit
	exit()
